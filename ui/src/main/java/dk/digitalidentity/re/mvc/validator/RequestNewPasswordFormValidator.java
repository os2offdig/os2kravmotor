package dk.digitalidentity.re.mvc.validator;

import dk.digitalidentity.re.dao.VendorUserDao;
import dk.digitalidentity.re.mvc.form.PasswordForm;
import dk.digitalidentity.re.mvc.form.RequestNewPasswordForm;
import dk.digitalidentity.re.service.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

@Component
public class RequestNewPasswordFormValidator implements Validator {

	@Autowired
	VendorUserDao vendorUserDao;

	@Override
	public boolean supports(Class<?> aClass) {
		return (RequestNewPasswordForm.class.isAssignableFrom(aClass));
	}

	@Override
	public void validate(Object o, Errors errors) {
		var email = ((RequestNewPasswordForm) o).getEmail();
		var vendorUser = vendorUserDao.getByEmail(email);
		if( vendorUser == null)
			errors.rejectValue("email", "error.vendor.requestnewpassword.usernotfound");

	}
}
