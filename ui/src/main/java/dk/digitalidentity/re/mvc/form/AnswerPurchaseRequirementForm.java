package dk.digitalidentity.re.mvc.form;

import lombok.Data;

@Data
public class AnswerPurchaseRequirementForm {
	private String choice;
	private String price;
	private String detail;
}
