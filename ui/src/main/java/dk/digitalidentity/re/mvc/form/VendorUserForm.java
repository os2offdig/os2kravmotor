package dk.digitalidentity.re.mvc.form;

import dk.digitalidentity.re.dao.model.LocalAttachment;
import dk.digitalidentity.re.dao.model.Purchase;
import dk.digitalidentity.re.dao.model.PurchaseAnswer;
import dk.digitalidentity.re.dao.model.enums.Importance;
import dk.digitalidentity.re.mvc.dto.AttachmentDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
public class VendorUserForm {
	private long id;
	private String email;
	private boolean admin;

	private List<Long> purchaseAnswerIds = new ArrayList<>();
	private List<PurchaseAnswer> purchaseAnswers = new ArrayList<>();
}