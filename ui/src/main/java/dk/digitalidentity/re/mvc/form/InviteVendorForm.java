package dk.digitalidentity.re.mvc.form;

import lombok.Data;

@Data
public class InviteVendorForm {
	private String email;

	private String message;
}
