package dk.digitalidentity.re.mvc.form;

import lombok.Data;

@Data
public class RequestNewPasswordForm {
	private String email;
}
