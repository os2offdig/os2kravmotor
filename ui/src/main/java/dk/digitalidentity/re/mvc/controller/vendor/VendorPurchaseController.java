package dk.digitalidentity.re.mvc.controller.vendor;

import dk.digitalidentity.re.dao.RequirementDao;
import dk.digitalidentity.re.dao.RequirementExtensionDao;
import dk.digitalidentity.re.dao.model.PurchaseAnswer;
import dk.digitalidentity.re.dao.model.PurchaseRequirement;
import dk.digitalidentity.re.dao.model.PurchaseRequirementAnswer;
import dk.digitalidentity.re.dao.model.enums.CustomerSetting;
import dk.digitalidentity.re.dao.model.enums.Status;
import dk.digitalidentity.re.mvc.dto.OtherPurchaseAnswerDTO;
import dk.digitalidentity.re.mvc.form.VendorPurchaseForm;
import dk.digitalidentity.re.mvc.view.RequirementXlsView;
import dk.digitalidentity.re.security.RequireVendorUserRole;
import dk.digitalidentity.re.service.*;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static dk.digitalidentity.re.utility.NullChecker.getValue;

@Controller
@Slf4j
@RequireVendorUserRole
public class VendorPurchaseController {

    @Value("${email.sender}")
    private String senderEmailAddress;

    @Autowired
    MailSenderService mailSenderService;

    @Autowired
    VendorOrganizationService vendorOrganizationService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    PurchaseService purchaseService;

    @Autowired
    SettingService settingService;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    ItSystemService itSystemService;

    @Autowired
    RequirementDao requirementDao;

    @Autowired
    RequirementExtensionDao requirementExtensionDao;


    @RequestMapping(path = "/vendor/purchase/list", method = RequestMethod.GET)
    public String list(Model model) {
        var purchaseAnswers = vendorOrganizationService.getVendorUser().getPurchaseAnswers();
        var vendorPurchaseForms = new ArrayList<VendorPurchaseForm>();
        for (var purchaseAnswer : purchaseAnswers) {
            var vendorPurchaseForm = new VendorPurchaseForm();
            vendorPurchaseForm.setPurchaseAnswer(purchaseAnswer);
            vendorPurchaseForm.setCustomerName(purchaseService.getCustomerNameByCvr(purchaseAnswer.getPurchase().getCvr()));
            vendorPurchaseForms.add(vendorPurchaseForm);
        }
        model.addAttribute("purchaseForms", vendorPurchaseForms);
        return "vendor/purchase/list";
    }

    @RequestMapping(path = "/vendor/purchase/edit/{purchaseAnswerId}", method = RequestMethod.GET)
    public String editPurchaseAnswer(Model model, @PathVariable("purchaseAnswerId") long purchaseAnswerId) {
        var purchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(purchaseAnswerId);

        var otherPurchaseAnswerDTOS = new ArrayList<OtherPurchaseAnswerDTO>();
        var otherPurchaseAnswers = vendorOrganizationService.getVendorUser().getPurchaseAnswers().stream().filter(pa -> pa.getId() != purchaseAnswer.getId()).collect(Collectors.toList());
        for (PurchaseAnswer otherPurchaseAnswer : otherPurchaseAnswers) {
            var otherPurchaseAnswerDTO = new OtherPurchaseAnswerDTO();
            otherPurchaseAnswerDTO.setId(otherPurchaseAnswer.getId());
            otherPurchaseAnswerDTO.setTitle(otherPurchaseAnswer.getPurchase().getTitle());
            otherPurchaseAnswerDTO.setCustomer(purchaseService.getCustomerNameByCvr(otherPurchaseAnswer.getPurchase().getCvr()));
            if (otherPurchaseAnswer.getItSystem() != null) {
                otherPurchaseAnswerDTO.setItSystem(otherPurchaseAnswer.getItSystem().getName() + " (" + otherPurchaseAnswer.getItSystem().getVendor() + ")");
            }
            otherPurchaseAnswerDTOS.add(otherPurchaseAnswerDTO);
        }
        model.addAttribute("purchaseAnswer", purchaseAnswer);
        model.addAttribute("otherPurchaseAnswers", otherPurchaseAnswerDTOS);
        model.addAttribute("itsystems", itSystemService.findAll());
        return "vendor/purchase/edit";
    }

    @RequestMapping(path = "/vendor/purchase/purchasemaster/{purchaseAnswerId}", method = RequestMethod.GET)
    public String getPurchaseMaster(Model model, @PathVariable("purchaseAnswerId") long purchaseAnswerId) {
        var purchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(purchaseAnswerId);
        model.addAttribute("purchaseAnswer", purchaseAnswer);
        model.addAttribute("reqAnswered", purchaseAnswer.getPurchaseRequirementAnswers().stream().filter(pa -> pa.isAnswered()).count());
        model.addAttribute("reqTotal", purchaseAnswer.getPurchaseRequirementAnswers().size());
        model.addAttribute("reqDirty", purchaseAnswer.getPurchaseRequirementAnswers().stream().filter(pa -> pa.isDirtyCopy()).count());
        model.addAttribute("askForPrice", settingService.getBooleanValueByKeyAndCvr(CustomerSetting.ASK_VENDOR_FOR_PRICE, purchaseAnswer.getPurchase().getCvr()));
        return "vendor/purchase/purchasemaster";
    }

    @RequestMapping(path = "/vendor/purchase/purchaserequirementlist/{purchaseAnswerId}", method = RequestMethod.GET)
    public String getPurchaseRequirementList(Model model, @PathVariable("purchaseAnswerId") long purchaseAnswerId) {
        var purchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(purchaseAnswerId);
        model.addAttribute("purchaseAnswer", purchaseAnswer);
        model.addAttribute("askForPrice", settingService.getBooleanValueByKeyAndCvr(CustomerSetting.ASK_VENDOR_FOR_PRICE, purchaseAnswer.getPurchase().getCvr()));
        return "vendor/purchase/purchaserequirementlist";
    }

    @ResponseBody
    @RequestMapping(path = "/vendor/purchase/edit/{purchaseAnswerId}/setitsystem/{itSystemId}", method = RequestMethod.POST)
    public ResponseEntity<String> setItSystem(@PathVariable("purchaseAnswerId") long purchaseAnswerId, @PathVariable("itSystemId") long itSystemId) {
        try {
            var purchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(purchaseAnswerId);
            if (!purchaseAnswer.getPurchase().getStatus().equals(Status.ACTIVE)) {
                throw new Exception("Attempt to set it-system on inactive purchase");
            }
            var itSystem = itSystemService.getById(itSystemId);
            purchaseAnswer.setItSystem(itSystem);
            vendorOrganizationService.savePurchaseAnswer(purchaseAnswer);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Could not set it system om purchase.", e);
            return new ResponseEntity<>("Error while processing request.", HttpStatus.BAD_REQUEST);
        }
    }

    @ResponseBody
    @RequestMapping(path = "/vendor/purchase/edit/{purchaseAnswerId}/copyanswers/{otherPurchaseAnswerId}", method = RequestMethod.POST)
    public ResponseEntity<String> copyAnswers(@PathVariable("purchaseAnswerId") long purchaseAnswerId, @PathVariable("otherPurchaseAnswerId") long otherPurchaseAnswerId) {
        try {
            var purchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(purchaseAnswerId);
            if (!purchaseAnswer.getPurchase().getStatus().equals(Status.ACTIVE)) {
                throw new Exception("Attempt to copy answers to inactive purchase");
            }

            var otherPurchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(otherPurchaseAnswerId);
            // copy answers to requirements that are not already answered and that are not custom requirements (requirementId 0)
            purchaseAnswer.getPurchaseRequirementAnswers().stream().filter(dest -> !dest.isAnswered() && dest.getRequirement().getRequirementId() != 0).forEach(dest -> {
                otherPurchaseAnswer.getPurchaseRequirementAnswers().stream()
                        .filter(source -> source.getRequirement().getRequirementId() == dest.getRequirement().getRequirementId() && source.isAnswered())
                        .findFirst().ifPresent(source -> {
                    dest.setAnswered(true);
                    dest.setChoice(source.getChoice());
                    dest.setDetail(source.getDetail());
                    dest.setPrice(source.getPrice());
                    // if the requirement being copied has been changed the copy should be marked as dirty
                    dest.setDirtyCopy(shouldFlagDirty(dest.getRequirement(), source.getRequirement()));
                });
            });
            vendorOrganizationService.savePurchaseAnswer(purchaseAnswer);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Could not copy answers.", e);
            return new ResponseEntity<>("Error while processing request.", HttpStatus.BAD_REQUEST);
        }
    }

    private boolean shouldFlagDirty(PurchaseRequirement destRequirement, PurchaseRequirement sourceRequirement) {
        if (!getValue(() -> destRequirement.getName(), "").equalsIgnoreCase(getValue(() -> sourceRequirement.getName(), ""))) {
            return true;
        }
        if (!getValue(() -> destRequirement.getDescription(), "").equalsIgnoreCase(getValue(() -> sourceRequirement.getDescription(), ""))) {
            return true;
        }
        if (!getValue(() -> destRequirement.getRationale(), "").equalsIgnoreCase(getValue(() -> sourceRequirement.getRationale(), ""))) {
            return true;
        }
        return false;
    }


    @RequireVendorUserRole
    @ResponseBody
    @RequestMapping(path = "/vendor/purchase/edit/{purchaseAnswerId}/clearitsystem", method = RequestMethod.POST)
    public ResponseEntity<String> clearItSystem(@PathVariable("purchaseAnswerId") long purchaseAnswerId) {
        try {
            var purchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(purchaseAnswerId);
            if (!purchaseAnswer.getPurchase().getStatus().equals(Status.ACTIVE)) {
                throw new Exception("Attempt to clear it-system on inactive purchase");
            }

            purchaseAnswer.setItSystem(null);
            vendorOrganizationService.savePurchaseAnswer(purchaseAnswer);
            return new ResponseEntity<String>(HttpStatus.OK);
        } catch (Exception e) {
            log.error("Could not clear it-system.", e);
            return new ResponseEntity<>("Error while processing request.", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(path = "vendor/purchase/answer/{purchaseRequirementAnswerId}", method = RequestMethod.GET)
    @RequireVendorUserRole
    public String editPurchaseRequirementAnswer(Model model, @PathVariable("purchaseRequirementAnswerId") long purchaseRequirementAnswerId) {
        var purchaseRequirementAnswer = vendorOrganizationService.getPurchaseRequirementAnswerById(purchaseRequirementAnswerId);
        model.addAttribute("purchaseRequirementAnswer", purchaseRequirementAnswer);
        model.addAttribute("attachments", vendorOrganizationService.getAttachments(purchaseRequirementAnswer));
        model.addAttribute("askForPrice", settingService.getBooleanValueByKeyAndCvr(CustomerSetting.ASK_VENDOR_FOR_PRICE, purchaseRequirementAnswer.getPurchaseAnswer().getPurchase().getCvr()));
        model.addAttribute("next", getNextPurchaseRequirementAnswerId(purchaseRequirementAnswer));
        model.addAttribute("previous", getPreviousPurchaseRequirementAnswerId(purchaseRequirementAnswer));
        return "vendor/purchase/answer";
    }

    @ResponseBody
    @RequireVendorUserRole
    @RequestMapping(path = "/vendor/purchase/answer", method = RequestMethod.POST)
    public ResponseEntity<String> savePurchaseRequirementAnswer(Model model, @ModelAttribute("purchaseRequirementAnswer") @Valid PurchaseRequirementAnswer purchaseRequirementAnswerForm) {
        try {
            var purchaseRequirementAnswer = vendorOrganizationService.getPurchaseRequirementAnswerById(purchaseRequirementAnswerForm.getId());
            if (!purchaseRequirementAnswer.getPurchaseAnswer().getPurchase().getStatus().equals(Status.ACTIVE)) {
                throw new Exception("Attempt to save purchase requirement answer on inactive purchase");
            }

            purchaseRequirementAnswer.setAnswered(purchaseRequirementAnswerForm.getChoice() != null);
            purchaseRequirementAnswer.setChoice(purchaseRequirementAnswerForm.getChoice());
            purchaseRequirementAnswer.setDetail(purchaseRequirementAnswerForm.getDetail());
            purchaseRequirementAnswer.setPrice(purchaseRequirementAnswerForm.getPrice());
            purchaseRequirementAnswer.setDirtyCopy(false);
            vendorOrganizationService.savePurchaseRequirementAnswer(purchaseRequirementAnswer);
            return new ResponseEntity<String>("", HttpStatus.OK);
        } catch (Exception e) {
            log.error("Could not save purchase requirement answer.", e);
            return new ResponseEntity<>("Error while processing request.", HttpStatus.BAD_REQUEST);
        }
    }

    private Long getNextPurchaseRequirementAnswerId(PurchaseRequirementAnswer purchaseRequirementAnswer) {
        var purchaseRequirementAnswers = purchaseRequirementAnswer.getPurchaseAnswer().getPurchaseRequirementAnswers();
        var iterator = purchaseRequirementAnswers.listIterator(purchaseRequirementAnswers.indexOf(purchaseRequirementAnswer) + 1);
        return iterator.hasNext() ? iterator.next().getId() : null;
    }

    private Long getPreviousPurchaseRequirementAnswerId(PurchaseRequirementAnswer purchaseRequirementAnswer) {
        var purchaseRequirementAnswers = purchaseRequirementAnswer.getPurchaseAnswer().getPurchaseRequirementAnswers();
        var iterator = purchaseRequirementAnswers.listIterator(purchaseRequirementAnswers.indexOf(purchaseRequirementAnswer));
        return iterator.hasPrevious() ? iterator.previous().getId() : null;
    }

    @RequireVendorUserRole
    @RequestMapping(path = "/vendor/purchase/download/{purchaseAnswerId}", method = RequestMethod.GET)
    public ModelAndView downloadRequirements(@PathVariable("purchaseAnswerId") long purchaseAnswerId, HttpServletResponse response, Locale loc) {
        var purchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(purchaseAnswerId);

        Map<String, Object> model = new HashMap<>();
        model.put("requirements", purchaseAnswer.getPurchase().getRequirements());
        model.put("messagesBundle", messageSource);
        model.put("locale", loc);

        response.setContentType("application/ms-excel");
        response.setHeader("Content-Disposition", "attachment; filename=\"krav.xls\"");

        return new ModelAndView(new RequirementXlsView(), model);
    }

    @RequireVendorUserRole
    @RequestMapping(path = "/vendor/purchase/exit/{purchaseAnswerId}", method = RequestMethod.GET)
    public String exitPurchase(@PathVariable("purchaseAnswerId") long purchaseAnswerId, Locale loc, Model model) {
        var purchaseAnswer = vendorOrganizationService.getPurchaseAnswerById(purchaseAnswerId);

        if (!purchaseAnswer.getPurchase().getStatus().equals(Status.ACTIVE)) {
            log.warn("Attempt to exit purchase with id " + purchaseAnswer.getPurchase().getId() + " that was not Active");
            model.addAttribute("status", purchaseAnswer.getPurchase().getStatus());
            return "vendor/notactive";
        }

        vendorOrganizationService.deletePurchaseAnswer(purchaseAnswer);

        String subject = messageSource.getMessage("email.vendor.exit.title", new String[]{purchaseAnswer.getPurchase().getTitle(), purchaseAnswer.getVendorOrganization().getDomain()}, loc);
        String body = messageSource.getMessage("email.vendor.exit.body", new String[]{purchaseAnswer.getPurchase().getTitle(), purchaseAnswer.getVendorOrganization().getDomain()}, loc);
        try {
            mailSenderService.sendMessage(senderEmailAddress, purchaseAnswer.getPurchase().getEmail(), subject, body);
        } catch (Exception ex) {
            log.warn("Error occured while trying to send email. ", ex);
        }

        return "redirect:/vendor/purchase/list";
    }

}
