package dk.digitalidentity.re.mvc.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OtherPurchaseAnswerDTO {
    public long id;
    public String title;
    public String itSystem;
    public String customer;
}
