package dk.digitalidentity.re.mvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import dk.digitalidentity.re.dao.CategoryDao;
import dk.digitalidentity.re.dao.PurchaseRequirementDao;
import dk.digitalidentity.re.dao.RequirementDao;
import dk.digitalidentity.re.dao.model.Category;
import dk.digitalidentity.re.mvc.dto.DeleteStatus;
import dk.digitalidentity.re.mvc.form.CategoryForm;
import dk.digitalidentity.re.mvc.validator.CategoryFormValidator;
import dk.digitalidentity.re.security.RequireEditorRole;
import dk.digitalidentity.re.security.RequireGlobalEditorRole;

@Controller
@RequireEditorRole
public class CategoryController {
	private static final Logger log = LoggerFactory.getLogger(CategoryController.class);
	
	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private CategoryDao categoryDao;

	@Autowired
	private RequirementDao requirementDao;

	@Autowired
	private PurchaseRequirementDao purchaseRequirementDao;

	@Autowired
	private CategoryFormValidator categoryFormValidator;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(categoryFormValidator);
	}

	@RequestMapping(path = { "category/", "category/list" }, method = RequestMethod.GET)
	public String listCategory(Model model) {
		List<CategoryForm> categories = modelMapper.map(categoryDao.findAll(), new TypeToken<List<CategoryForm>>() {}.getType());

		model.addAttribute("categories", categories);
		model.addAttribute("category", new CategoryForm());
		model.addAttribute("categoryEdit", new CategoryForm());

		return "category/list";
	}

	@RequireGlobalEditorRole
	@RequestMapping(path = { "category/" }, method = RequestMethod.POST)
	public String newCategory(Model model, @ModelAttribute("category") @Valid CategoryForm categoryForm, BindingResult bindingResult) {
		if (bindingResult.hasErrors()) {
			model.addAttribute(bindingResult.getAllErrors());
			model.addAttribute("category", categoryForm);
			model.addAttribute("categoryEdit", new CategoryForm());
			model.addAttribute("categories", modelMapper.map(categoryDao.findAll(), new TypeToken<List<CategoryForm>>() {}.getType()));

			return "category/list";
		}

		List<CategoryForm> categories = modelMapper.map(categoryDao.findAll(), new TypeToken<List<CategoryForm>>() {}.getType());
		Category newCategory = modelMapper.map(categoryForm, Category.class);
		categoryDao.save(newCategory);

		model.addAttribute("categories", categories);

		return "redirect:/category/";
	}

	@RequireGlobalEditorRole
	@RequestMapping(path = "category/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseEntity<String> deleteCategory(@PathVariable("id") long id) {
		Category category = categoryDao.getById(id);

		if (category == null) {
			log.warn("Requested Category with ID:"+id+ " not found.");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		categoryDao.delete(category);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	@RequireGlobalEditorRole
	@RequestMapping(path = { "category/edit/" }, method = RequestMethod.POST)
	public String editCategory(Model model, @ModelAttribute("categoryEdit") @Valid CategoryForm categoryForm, BindingResult bindingResult) {
		Category category = categoryDao.getById(categoryForm.getId());
		if (category == null) {
			log.warn("Requested Category with ID:" + categoryForm.getId() + " not found.");
	                return "redirect:/category/";
		}

		if (bindingResult.hasErrors()) {
			model.addAttribute(bindingResult.getAllErrors());
			model.addAttribute("state", "edit");
			model.addAttribute("category", new CategoryForm());
			model.addAttribute("categoryEdit", categoryForm);
			model.addAttribute("categories", modelMapper.map(categoryDao.findAll(), new TypeToken<List<CategoryForm>>() {}.getType()));

			return "category/list";
		}

		category.setName(categoryForm.getName());
		categoryDao.save(category);

		return "redirect:/category/";
	}

	@RequireGlobalEditorRole
	@RequestMapping(value = "/category/trydelete/{id}", method = RequestMethod.GET)
	public ResponseEntity<DeleteStatus> tryDelete(@PathVariable("id") long id) {
		Category category = categoryDao.getById(id);
		if (category == null) {
			log.warn("Cannot find category with id = " + id);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		DeleteStatus status = new DeleteStatus();

		// Find all requirement that use this category
		long requirementsQuantity = requirementDao.countByCategory(category);
		long purchaseQuantity = purchaseRequirementDao.countByCategory(category);

		if (requirementsQuantity < 1  && purchaseQuantity < 1) {
			status.setSuccess(true);

			return new ResponseEntity<>(status, HttpStatus.OK);
		}

		if (requirementsQuantity > 0) {
			status.setRequirementQuantity(requirementsQuantity);
		}

		if (purchaseQuantity > 0) {
			status.setPurchaseQuantity(purchaseQuantity);
		}

		status.setSuccess(false);
		
		return new ResponseEntity<>(status, HttpStatus.OK);
	}
}
