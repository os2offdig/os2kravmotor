package dk.digitalidentity.re.mvc.form;

import lombok.Data;

@Data
public class CategoryForm {
	private long id;
	private String name;
}
