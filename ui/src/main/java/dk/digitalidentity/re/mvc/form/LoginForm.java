package dk.digitalidentity.re.mvc.form;

import lombok.Data;

@Data
public class LoginForm {
	private String username;
	private String password;
}
