package dk.digitalidentity.re.mvc.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttachmentDTO {
	private long id;
	private String name;
	private String url;
}
