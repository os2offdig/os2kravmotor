package dk.digitalidentity.re.mvc.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import dk.digitalidentity.re.mvc.dto.JIRASprintDTO;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
public class JIRASprintsDTO {
	private List<JIRASprintDTO> values;
}
