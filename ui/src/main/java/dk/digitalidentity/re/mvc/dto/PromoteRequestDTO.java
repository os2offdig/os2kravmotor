package dk.digitalidentity.re.mvc.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PromoteRequestDTO {
	String target;
}
