package dk.digitalidentity.re.mvc.dto;

import lombok.Data;

@Data
public class UpdatePurchaseStatus {
	private String status;
}
