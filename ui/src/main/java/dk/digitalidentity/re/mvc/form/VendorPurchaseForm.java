package dk.digitalidentity.re.mvc.form;

import dk.digitalidentity.re.dao.model.PurchaseAnswer;
import dk.digitalidentity.re.dao.model.PurchaseVendor;
import dk.digitalidentity.re.dao.model.enums.Status;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class VendorPurchaseForm {
	private String customerName;
	private PurchaseAnswer purchaseAnswer;
}
