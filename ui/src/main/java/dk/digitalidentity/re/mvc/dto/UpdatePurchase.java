package dk.digitalidentity.re.mvc.dto;

import lombok.Data;

@Data
public class UpdatePurchase {
	private String field;
}