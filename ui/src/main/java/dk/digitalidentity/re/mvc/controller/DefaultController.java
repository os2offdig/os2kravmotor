package dk.digitalidentity.re.mvc.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.opensaml.common.SAMLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import dk.digitalidentity.re.dao.model.Requirement;
import dk.digitalidentity.re.mvc.view.GlobalRequirementXlsView;
import dk.digitalidentity.re.service.RequirementService;

@Controller
public class DefaultController implements ErrorController {
	private ErrorAttributes errorAttributes = new DefaultErrorAttributes();

	@Autowired
	private RequirementService requirementService;

	@Autowired
	private MessageSource messageSource;

	@Value(value = "${error.showtrace:false}")
	private boolean showStackTrace;

	@RequestMapping(value = { "/", "/ui/" })
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/error", produces = "text/html")
	public String errorPage(Model model, HttpServletRequest request) {
		Map<String, Object> body = getErrorAttributes(new ServletWebRequest(request), showStackTrace);

		// deal with SAML errors first
		Object status = body.get("status");
		if (status != null && status instanceof Integer && (Integer) status == 999) {
			Object authException = request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);

			// handle the forward case
			if (authException == null && request.getSession() != null) {
				authException = request.getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
			}

			if (authException != null && authException instanceof Throwable) {
				StringBuilder builder = new StringBuilder();
				Throwable t = (Throwable) authException;

				logThrowable(builder, t, false);
				model.addAttribute("exception", builder.toString());

				if (t.getCause() != null) {
					t = t.getCause();

					// deal with the known causes for this error
					if (t instanceof SAMLException) {
						if (t.getCause() != null && t.getCause() instanceof CredentialsExpiredException) {
							model.addAttribute("cause", "EXPIRED");
						}
						else if (t.getMessage() != null && t.getMessage().contains("Response issue time is either too old or with date in the future")) {
							model.addAttribute("cause", "SKEW");
						}
						else if (t.getMessage() != null && t.getMessage().contains("urn:oasis:names:tc:SAML:2.0:status:Responder")) {
							model.addAttribute("cause", "RESPONDER");
						}
						else {
							model.addAttribute("cause", "UNKNOWN");
						}
					}
					else {
						model.addAttribute("cause", "UNKNOWN");
					}
				}

				return "error/samlerror";
			}
		}

		// default to ordinary error message in case error is not SAML related
		model.addAllAttributes(body);

		return "error/error";
	}

	private void logThrowable(StringBuilder builder, Throwable t, boolean append) {
		StackTraceElement[] stackTraceElements = t.getStackTrace();

		builder.append((append ? "Caused by: " : "") + t.getClass().getName() + ": " + t.getMessage() + "\n");
		for (int i = 0; i < 5 && i < stackTraceElements.length; i++) {
			builder.append("  ... " + stackTraceElements[i].toString() + "\n");
		}

		if (t.getCause() != null) {
			logThrowable(builder, t.getCause(), true);
		}
	}

	@RequestMapping(value = "/error", produces = "application/json")
	public ResponseEntity<Map<String, Object>> errorJSON(HttpServletRequest request) {
		Map<String, Object> body = getErrorAttributes(new ServletWebRequest(request), showStackTrace);

		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
		try {
			status = HttpStatus.valueOf((int) body.get("status"));
		}
		catch (Exception ex) {
			;
		}

		return new ResponseEntity<>(body, status);
	}

	@RequestMapping(path = "/download/requirements")
	public ModelAndView downloadGlobalRequirements(HttpServletResponse response, Locale loc) {
		List<Requirement> requirements = requirementService.getAllRequirementsOfType(RequirementService.RequirementType.GLOBAL);

		Map<String, Object> model = new HashMap<>();
		model.put("requirements", requirements);
		model.put("messagesBundle", messageSource);
		model.put("locale", loc);

		response.setContentType("application/ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=\"krav.xls\"");

		return new ModelAndView(new GlobalRequirementXlsView(), model);
	}
	
	@Override
	public String getErrorPath() {
		return "/_dummyErrorPage";
	}

	private Map<String, Object> getErrorAttributes(WebRequest request, boolean includeStackTrace) {
		return errorAttributes.getErrorAttributes(request, includeStackTrace);
	}
}
