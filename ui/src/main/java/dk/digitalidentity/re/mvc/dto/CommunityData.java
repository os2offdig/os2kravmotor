package dk.digitalidentity.re.mvc.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommunityData {
	private long id;
	private String name;
	private boolean selected;
}
