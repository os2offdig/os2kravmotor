package dk.digitalidentity.re.dao;

import dk.digitalidentity.re.dao.model.VendorUser;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface VendorUserDao extends CrudRepository<VendorUser, Long> {
    VendorUser getById(long id);
    VendorUser getByEmail(String email);
}
