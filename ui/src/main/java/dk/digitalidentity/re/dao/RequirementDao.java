package dk.digitalidentity.re.dao;

import org.springframework.data.repository.CrudRepository;

import dk.digitalidentity.re.dao.model.ArchitecturePrinciple;
import dk.digitalidentity.re.dao.model.Category;
import dk.digitalidentity.re.dao.model.Domain;
import dk.digitalidentity.re.dao.model.Requirement;
import dk.digitalidentity.re.dao.model.Tag;

import java.util.Collection;
import java.util.List;

public interface RequirementDao extends CrudRepository<Requirement, Long> {
	long countByCategory(Category category);
	long countByDomainsContains(Domain domain);
	long countByTagsContains(Tag tag);
	long countByPrinciplesContains(ArchitecturePrinciple principle);
	Requirement getById(long id);	
	List<Requirement> findAll();
	List<Requirement> findByDomainsContainsAndCvrIn(Domain domain, Collection<String> cvrs);
	List<Requirement> findByTagsContainsAndCvrIn(Tag tag, Collection<String> cvrs);
	List<Requirement> findByAvailableForAllTagsTrueAndCvrIn(Collection<String> cvrs);
	List<Requirement> findByCvrIn(Collection<String> cvrs);
	List<Requirement> findByRequestedToBeSharedTrue();
}
