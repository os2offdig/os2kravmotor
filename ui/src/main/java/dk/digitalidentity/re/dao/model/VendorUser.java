package dk.digitalidentity.re.dao.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import dk.digitalidentity.re.log.EventLoggable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class VendorUser implements EventLoggable {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String email;

	@Column
	private String password;

	@Column
	private boolean admin;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "vendor_organization_id")
	private VendorOrganization vendorOrganization;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "vendor_user_purchase_answer", joinColumns = { @JoinColumn(name = "vendor_user_id") }, inverseJoinColumns = { @JoinColumn(name = "purchase_answer_id") })
	private List<PurchaseAnswer> purchaseAnswers = new ArrayList<>();

	@Override
	public String getEntityId() {
		return Long.toString(id);
	}
}
