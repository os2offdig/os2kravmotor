package dk.digitalidentity.re.dao.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import dk.digitalidentity.re.log.EventLoggable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class VendorOrganization implements EventLoggable {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private String domain;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "vendor_organization_id")
	private List<PurchaseAnswer> purchaseAnswers = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "vendor_organization_id")
	private List<VendorUser> vendorUsers = new ArrayList<>();

	@Override
	public String getEntityId() {
		return Long.toString(id);
	}
}
