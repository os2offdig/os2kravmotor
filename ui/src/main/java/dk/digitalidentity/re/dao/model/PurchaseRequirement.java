package dk.digitalidentity.re.dao.model;

import dk.digitalidentity.re.dao.model.enums.Importance;
import dk.digitalidentity.re.log.EventLoggable;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Data
@ToString(exclude = { "purchase" })
@Entity(name = "purchase_requirement")
public class PurchaseRequirement implements EventLoggable {

	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column
	private long requirementId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "purchase_id")
	private Purchase purchase;

	@Column
	private String name;

	@Column
	private String description;

	@Column
	private String rationale;

	@Column
	private boolean infoRequirement;

	@Temporal(TemporalType.TIMESTAMP)
	@Column
	private Date lastChanged;

	@OneToOne
	private Category category;

	@Column
	@Enumerated(EnumType.STRING)
	private Importance importance;

	@Override
	public String getEntityId() {
		return Long.toString(id);
	}
}
