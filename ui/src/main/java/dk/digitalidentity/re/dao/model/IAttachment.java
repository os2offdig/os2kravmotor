package dk.digitalidentity.re.dao.model;

public interface IAttachment {
    String getName();
    String getUrl();
}
