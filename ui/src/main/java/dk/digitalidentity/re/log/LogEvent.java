package dk.digitalidentity.re.log;

public enum LogEvent {
	CREATE, UPDATE, DELETE;
}
