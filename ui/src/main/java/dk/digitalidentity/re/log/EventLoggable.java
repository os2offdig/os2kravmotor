package dk.digitalidentity.re.log;

public interface EventLoggable {
	String getEntityId();
}
