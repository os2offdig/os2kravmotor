package dk.digitalidentity.re.config;

import dk.digitalidentity.re.dao.VendorUserDao;
import dk.digitalidentity.re.dao.model.VendorUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import dk.digitalidentity.re.dao.PurchaseVendorDao;
import dk.digitalidentity.re.security.VendorLoginFilter;

@Configuration
public class VendorLoginFilterConfiguration {
	
	@Autowired
	private PurchaseVendorDao purchaseVendorDao;

	@Autowired
	private VendorUserDao vendorUserDao;

	@Bean
	public FilterRegistrationBean vendorLoginFilter() {
		VendorLoginFilter filter = new VendorLoginFilter(purchaseVendorDao, vendorUserDao);

		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(filter);
		filterRegistrationBean.addUrlPatterns("/vendor/*");

		return filterRegistrationBean;
	}
}
