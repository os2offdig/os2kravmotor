package dk.digitalidentity.re.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import dk.digitalidentity.re.dao.model.Attachment;
import dk.digitalidentity.re.dao.model.LocalAttachment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dk.digitalidentity.re.Constants;
import dk.digitalidentity.re.dao.RequirementDao;
import dk.digitalidentity.re.dao.RequirementExtensionDao;
import dk.digitalidentity.re.dao.model.Community;
import dk.digitalidentity.re.dao.model.CommunityMember;
import dk.digitalidentity.re.dao.model.Domain;
import dk.digitalidentity.re.dao.model.Requirement;
import dk.digitalidentity.re.dao.model.RequirementExtension;
import dk.digitalidentity.re.dao.model.Tag;
import dk.digitalidentity.re.mvc.form.RequirementForm;
import dk.digitalidentity.re.security.SecurityUtil;

@Service
public class RequirementService {
	public enum RequirementType { GLOBAL, LOCAL, TOBESHARED, COMMUNITY };

	@Autowired
	private RequirementDao requirementDao;

	@Autowired
	private RequirementExtensionDao requirementExtensionDao;

	@Autowired
	private CommunityService communityService;

	@Autowired
	private SecurityUtil securityUtil;

	public Requirement getById(long id) {
		Requirement requirement = requirementDao.getById(id);
		if (requirement != null && canRead(requirement)) {
			return requirement;
		}

		return null;
	}

	public List<Requirement> getByTagsContains(Tag tag) {
		String cvr = securityUtil.getCvr();
		List<Requirement> requirementsByCvr;

		List<String> communities = communityService.getCommunities(cvr).stream().map(Community::getCommunityCvr).collect(Collectors.toList());

		if (cvr != null) {
			requirementsByCvr = requirementDao.findByTagsContainsAndCvrIn(tag, Stream.concat(Arrays.asList(cvr, Constants.DEFAULT_CVR).stream(), communities.stream()).collect(Collectors.toList()));
		}
		else {
			requirementsByCvr = requirementDao.findByTagsContainsAndCvrIn(tag, Arrays.asList(Constants.DEFAULT_CVR));
		}

		return requirementsByCvr;
	}

	public List<Requirement> getByTagsAvailableForAll() {
		String cvr = securityUtil.getCvr();
		List<Requirement> requirementsByCvr;

		List<String> communities = communityService.getCommunities(cvr).stream().map(Community::getCommunityCvr).collect(Collectors.toList());

		if (cvr != null) {
			requirementsByCvr = requirementDao.findByAvailableForAllTagsTrueAndCvrIn(Stream.concat(Arrays.asList(cvr, Constants.DEFAULT_CVR).stream(), communities.stream()).collect(Collectors.toList()));
		}
		else {
			requirementsByCvr = requirementDao.findByAvailableForAllTagsTrueAndCvrIn(Arrays.asList(Constants.DEFAULT_CVR));
		}

		return requirementsByCvr;
	}

	public List<Requirement> getByDomainsContains(Domain domain) {
		String cvr = securityUtil.getCvr();
		List<Requirement> requirementsByCvr;

		List<String> communities = communityService.getCommunities(cvr).stream().map(Community::getCommunityCvr).collect(Collectors.toList());

		if (cvr != null) {
			requirementsByCvr = requirementDao.findByDomainsContainsAndCvrIn(domain, Stream.concat(Arrays.asList(cvr, Constants.DEFAULT_CVR).stream(), communities.stream()).collect(Collectors.toList()));
		}
		else {
			requirementsByCvr = requirementDao.findByDomainsContainsAndCvrIn(domain, Arrays.asList(Constants.DEFAULT_CVR));
		}

		return requirementsByCvr;
	}

	public void delete(Requirement requirement) {
		if (canModify(requirement)) {
			requirementDao.delete(requirement);
		} else {
			throw new IllegalStateException("User is not allowed to delete requirement");
		}
	}

	public List<Requirement> getAllRequirements() {
		String cvr = securityUtil.getCvr();

		List<String> communities = communityService.getCommunities(cvr).stream().map(Community::getCommunityCvr).collect(Collectors.toList());

		if (cvr != null) {
			return requirementDao.findByCvrIn(Stream.concat(Arrays.asList(cvr, Constants.DEFAULT_CVR).stream(), communities.stream()).collect(Collectors.toList()));
		}

		return requirementDao.findByCvrIn(Arrays.asList(Constants.DEFAULT_CVR));
	}

	public List<Requirement> getAllRequirementsOfType(RequirementType type) {
		String cvr = securityUtil.getCvr();

		if (type.equals(RequirementType.TOBESHARED)) {
			return requirementDao.findByRequestedToBeSharedTrue();
		}
		else if (cvr != null && type.equals(RequirementType.LOCAL)) {
			return requirementDao.findByCvrIn(Arrays.asList(cvr));
		}
		else if (type.equals(RequirementType.GLOBAL)) {
			return requirementDao.findByCvrIn(Arrays.asList(Constants.DEFAULT_CVR));
		}
		else if(type.equals(RequirementType.COMMUNITY)) {
			List<Community> communities = communityService.getCommunities(SecurityUtil.getMunicipalityCvr());
			return requirementDao.findByCvrIn(communities.stream().map(Community::getCommunityCvr).collect(Collectors.toList()));
		}

		return new ArrayList<>();
	}

	public void handlePromotionRequest(long id, boolean acceptRequirement) {
		Requirement requirement = getById(id);
		if (requirement == null || !canModify(requirement)) {
			throw new IllegalAccessError("User " + SecurityUtil.getUser() + " is not allowed to modify requirement " + id);
		}

		requirement.setRequestedToBeShared(false);
		if (acceptRequirement) {
			requirement.setCvr(Constants.DEFAULT_CVR);
		}

		requirementDao.save(requirement);
	}

	public void changeOwnership(long id, Community community) {
		Requirement requirement = getById(id);
		if (requirement == null || !canModify(requirement)) {
			throw new IllegalAccessError("User " + SecurityUtil.getUser() + " is not allowed to modify requirement " + id);
		}

		boolean allowed = false;
		for (CommunityMember member : community.getCommunityMembers()) {
			if (SecurityUtil.getMunicipalityCvr().equals(member.getMunicipalityCvr())) {
				allowed = true;
			}
		}

		if (!allowed) {
			throw new IllegalAccessError("User " + SecurityUtil.getUser() + " is not allowed to promote to community " + community.getCommunityCvr() + " for requirement " + id);
		}

		requirement.setCvr(community.getCommunityCvr());
		requirementDao.save(requirement);
	}

	public Requirement save(Requirement requirement) {
		if (requirement.getId() > 0) {
			Requirement requirementFromDB = getById(requirement.getId());
			if (requirementFromDB == null || !canModify(requirementFromDB)) {
				throw new IllegalAccessError("User " + SecurityUtil.getUser() + " is not allowed to modify requirement " + requirement.getId());
			}

			requirement.setCvr(requirementFromDB.getCvr());
		}
		else {
			requirement.setCvr(securityUtil.getCvr());
		}

		if (canModify(requirement)) {
			return requirementDao.save(requirement);
		}

		throw new IllegalStateException("No user is logged in, unable to save requirement");
	}

	public RequirementExtension getRequirementExtension(Requirement requirement) {
		return requirementExtensionDao.getByRequirementAndCvr(requirement, SecurityUtil.getMunicipalityCvr());
	}

	public  RequirementExtension getRequirementExtensionByRequirementID(long id) {
		return getRequirementExtension(getById(id));
	}

	public void updateLocalExtensionsOnly(RequirementForm requirement, List<Attachment> attachments, List<Long> attachmentsToBeRemoved) {
		if (requirement.getId() > 0) {
			Requirement req = getById(requirement.getId());

			if (req != null) {
				RequirementExtension extension = requirementExtensionDao.getByRequirementAndCvr(req, SecurityUtil.getMunicipalityCvr());

				if (extension != null) {
					extension.setDescription(requirement.getExtDescription());
					extension.setHelpText(requirement.getHelpText());

					for (Long id : attachmentsToBeRemoved) {
						extension.getAttachments().removeIf(a -> a.getId() == id);
					}

					extension.getAttachments().addAll(getAttachmentsAsLocalAttachments(attachments, extension));
				}
				else {
					extension = new RequirementExtension();
					extension.setCvr(SecurityUtil.getMunicipalityCvr());
					extension.setRequirement(req);
					extension.setDescription(requirement.getExtDescription());
					extension.setHelpText(requirement.getHelpText());
					extension.setAttachments(getAttachmentsAsLocalAttachments(attachments, extension));
				}

				requirementExtensionDao.save(extension);
				return;
			}
		}

		throw new IllegalStateException("Requirement with id " + requirement.getId() + " does not exist");
	}

	private List<LocalAttachment> getAttachmentsAsLocalAttachments(List<Attachment> attachments, RequirementExtension extension) {
		List<LocalAttachment> localAttachments = new ArrayList<>();
		for (Attachment a : attachments) {
			LocalAttachment attachment = new LocalAttachment();
			attachment.setName(a.getName());
			attachment.setRequirementExtension(extension);
			attachment.setUrl(a.getUrl());
			localAttachments.add(attachment);
		}
		return localAttachments;
	}

	private boolean canRead(Requirement requirement) {
		String cvr = securityUtil.getCvr();
		List<String> roles = SecurityUtil.getRoles();

		if (cvr.equals(requirement.getCvr())) {
			return true;
		}

		if (Constants.DEFAULT_CVR.equals(requirement.getCvr())) {
			return true;
		}

		// global editors can read requirements that are pending approval
		if (roles.contains("ROLE_http://kravmotoren.dk/globaleditor") && requirement.isRequestedToBeShared()) {
			return true;
		}

		// users can lookup requirements from communities
		//   get list of communities this user belongs to; filter only cvr;               toList of strings            check if contains cvr of requirement in question 
		if (communityService.getCommunities(cvr).stream().map(Community::getCommunityCvr).collect(Collectors.toList()).contains(requirement.getCvr())) {
			return true;
		}

		return false;
	}

	public boolean canModify(Requirement requirement) {
		String cvr = securityUtil.getCvr();
		List<String> roles = SecurityUtil.getRoles();

		// only editor can modify requirement
		if (!roles.contains("ROLE_http://kravmotoren.dk/editor")) {
			return false;
		}

		// only owner can modify
		if (requirement.getCvr() != null && !cvr.equals(requirement.getCvr())) {
			boolean illegalAccess = true;

			// only global editors can modify global requirement
			if (roles.contains("ROLE_http://kravmotoren.dk/globaleditor")) {

				if (Constants.DEFAULT_CVR.equals(requirement.getCvr())) {
					illegalAccess = false;
				}

				// global editors can edit requirements that have been requested to be shared
				else if (requirement.isRequestedToBeShared()) {
					illegalAccess = false;
				}
			}

			if (illegalAccess) {
				return false;
			}
		}

		return true;
	}
}
