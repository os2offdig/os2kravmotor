package dk.digitalidentity.re.security;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import dk.digitalidentity.re.dao.GlobalEditorDao;
import dk.digitalidentity.re.dao.model.GlobalEditor;
import dk.digitalidentity.saml.extension.SamlLoginPostProcessor;
import dk.digitalidentity.saml.model.TokenUser;

@Component
public class RolePostProcesser implements SamlLoginPostProcessor {

	@Autowired
	private GlobalEditorDao globalEditorDao;

	@Override
	public void process(TokenUser tokenUser) {
		List<GrantedAuthority> newAuthorities = new ArrayList<>();

		for (Iterator<? extends GrantedAuthority> iterator = tokenUser.getAuthorities().iterator(); iterator.hasNext();) {
			GrantedAuthority grantedAuthority = iterator.next();
			
			if ("ROLE_http://kravmotoren.dk/editor".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority("ROLE_http://kravmotoren.dk/editor"));
			}
			else if ("ROLE_http://kravmotoren.dk/purchaser".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority("ROLE_http://kravmotoren.dk/purchaser"));
			}
			else if ("ROLE_http://kravmotoren.dk/admin".equals(grantedAuthority.getAuthority())) {
				newAuthorities.add(new SimpleGrantedAuthority("ROLE_http://kravmotoren.dk/admin"));
			}
		}

		// add global editor role to configured set of global editors
		for (GlobalEditor globalEditor : globalEditorDao.findAll()) {
			if (tokenUser.getUsername().equals(globalEditor.getUserId()) && globalEditor.getCvr().equals(tokenUser.getCvr())) {
				newAuthorities.add(new SimpleGrantedAuthority("ROLE_http://kravmotoren.dk/globaleditor"));
			}
		}
		
		tokenUser.setAuthorities(newAuthorities);
	}
}
