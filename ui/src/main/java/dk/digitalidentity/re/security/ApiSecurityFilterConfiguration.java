package dk.digitalidentity.re.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import dk.digitalidentity.re.dao.SettingDao;

@Configuration
public class ApiSecurityFilterConfiguration {
	
	@Autowired
	private SettingDao settingDao;

	@Bean
	public FilterRegistrationBean apiSecurityFilter() {
		ApiSecurityFilter filter = new ApiSecurityFilter(settingDao);

		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(filter);
		filterRegistrationBean.addUrlPatterns("/api/*");

		return filterRegistrationBean;
	}
}
