CREATE TABLE architecture_principle (
  id                              BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  name                            VARCHAR(64) NOT NULL,
  reference                       TEXT NOT NULL
);