OS2kravmotor was developed by Digital Identity (http://www.digital-identity.dk) for OS2 - Offentligt digitaliseringsfællesskab (http://os2.eu).

Copyright (c) 2017, OS2 - Offentligt digitaliseringsfællesskab.

OS2kravmotor is free software; you may use, study, modify and
distribute it under the terms of version 2.0 of the Mozilla Public
License. See the LICENSE file for details. If a copy of the MPL was not
distributed with this file, You can obtain one at
http://mozilla.org/MPL/2.0/.

All source code in this and the underlying directories (except the webjar folder) is subject to
the terms of the Mozilla Public License, v. 2.0. 

The webjar folder contains a Twitter Bootstrap theme called Angle, which is distributed as binary only, and requires purchasing a license. A license has been purchased for the OS2kravmotor production environment, and is located in the webjar folder.

